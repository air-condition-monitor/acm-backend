# Backend for Air-Condition Monitor

### Example queries:

```
select * from "temperature" where time > now() - 5d
drop SERIES where "device" = 'Arduino'
select * from "temperature"
select count("value") from "temperature" where time > '2020-07-05T00:00:00Z' and time < '2020-07-06T00:00:00Z'
SELECT * FROM measurement ORDER BY time DESC LIMIT 10
```
