from setuptools import find_packages, setup

module_name = "server"

setup(
    name=module_name,
    version="2.0",
    description="Server for Air Condition Monitor",
    author="Severi Kujala",
    packages=find_packages(where="src"),
    package_dir={"": "src"},

    entry_points={
    },

    install_requires=[
        "Flask",
        "Flask-Cors",
        "influxdb",
        "gunicorn",
        "toml",
    ],

    extras_require={
        "test": [
            "wheel",
            "pytest",
            "requests",
        ],
    },

)

