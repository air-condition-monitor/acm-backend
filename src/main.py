from app import app
import sys


if __name__ == "__main__":

    port = 5000 if len(sys.argv) < 2 else int(sys.argv[1])

    # visible to other devices
    app.run(host="0.0.0.0", port=port)
