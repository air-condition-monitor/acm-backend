import logging
import os


def config_logging(log_config, projectDir, gunicorn, override_other=True):

    if override_other:
        remove_loggers()

    if gunicorn:
        logging.config.dictConfig(log_config)
    else:
        # log into logs/
        init_default(log_config, projectDir)


def init_default(log_config, projectDir):
    new_config = log_config.copy()

    path = new_config["handlers"]["file"]["filename"]
    logfile = path.split("/")[-1]
    new_config["handlers"]["file"]["filename"] = projectDir + "/logs/" + logfile

    if not os.path.exists(new_config["handlers"]["file"]["filename"]):
        try:
            index = new_config["handlers"]["file"]["filename"].rfind("/")
            dir_path = new_config["handlers"]["file"]["filename"][0:index]
            os.makedirs(dir_path)

        except:
            pass

        finally:
            # create empty file
            with open(new_config["handlers"]["file"]["filename"], "w") as f:
                pass

    logging.config.dictConfig(new_config)


def remove_loggers():
    logging._acquireLock()

    for logger in logging.Logger.manager.loggerDict.values():

        if not isinstance(logger, logging.Logger):
            continue

        logger.handlers.clear()        # unset all logger handlers
        logger.filters.clear()         # unset all logger filters
        logger.level = logging.NOTSET  # unset logger level
        logger.propagate = True        # enable logger propagation

    logging._releaseLock()

