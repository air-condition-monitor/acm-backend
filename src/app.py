from flask import Flask
from flask import jsonify
from flask import request
from flask import make_response
from flask.logging import default_handler
from flask_cors import CORS
from flask_cors import cross_origin

from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError
from contextlib import contextmanager

from datetime import datetime
from datetime import timedelta

import logging
import logging.config
import json
import itertools
import sys
import os
import os.path
import toml
import utility


projectDir = os.getcwd()

config = toml.load(f"{projectDir}/configs/config.toml", _dict=dict)
db_config = config.get("database")
log_config = toml.load(f"{projectDir}/configs/log.toml", _dict=dict)["logger"]


devices = dict()

for key in config["device"]:
    devices[key] = datetime.now()


#app = Flask(__name__, static_folder="/frontend/build", static_url_path="/")
app = Flask(__name__)
cors = CORS(app)
app.config["DEBUG"] = False
app.config["CORS_HEADERS"] = "Content-Type"

run_with_gunicorn = True if "gunicorn" in sys.argv[0] else False

utility.config_logging(log_config, projectDir, run_with_gunicorn, False)
log = logging.getLogger("api")
log.info(f"using gunicorn: {run_with_gunicorn}")


@contextmanager
def connection():

    try:
        host = db_config["influx1"]["host"] if run_with_gunicorn else "localhost"
        client = InfluxDBClient(host=host,
                                port=int(db_config["influx1"]["port"]),
                                database=db_config["influx1"]["name"],
                                username=db_config["influx1"]["user"])


        # TODO: create only on 1st connect
        # TODO: get_list_database() for checking if exists
        client.create_database(db_config["influx1"]["name"])

        client.switch_database(db_config["influx1"]["name"])
        yield client

    except Exception as err:
        log.exception("failed to connect to database")
        raise Exception


    finally:
        client.close()


@app.route("/")
def index():
    return app.send_static_file("index.html")


@app.route("/api/current/<location>/<device>")
def get_current(location, device):
    """Get newest data on specified device in specified location.

    Return:
        json of points

    """
    with connection() as client:
        result = client.query(query='SELECT * \
                                 FROM "all" \
                                 WHERE "device" = $device and "location" = $location \
                                 ORDER BY time DESC \
                                 LIMIT 1',
                          bind_params={"device": device, "location": location})


        if len(result) == 0:
            return jsonify([])

        points_dict =  next(result.get_points())
        points = [points_dict]

        return jsonify(points)


@app.route("/api/latest")
def get_latest():
    """Get n latest data points of given device and location.

    Args:
        count: int, how many points to retrieve
        location: str, where devices are located
        device: str, name of specific device, can be "any" to get all from specific location

    Return:
        json of data points
    """
    count  = request.args.get("count", default=1, type=int)
    location = request.args.get("location", default="any", type=str)
    device = request.args.get("device", default="any", type=str)

    if count <= 0:
        return jsonify([])

    with connection() as client:

        if device == "any":
            result = client.query(query='SELECT * \
                                     FROM "all" \
                                     WHERE "location" = $location \
                                     ORDER BY time DESC',
                              bind_params={"location": location})

        else:
            result = client.query(query='SELECT * \
                                     FROM "all" \
                                     WHERE "device" = $device and "location" = $location \
                                     ORDER BY time DESC',
                              bind_params={"device": device, "location": location})

        if len(result) == 0:
            return jsonify([])

        points = list(itertools.islice(result.get_points(), count))


        return jsonify(points)


@app.route("/api/range")
def get_range():
    """Get data points of given device and location between two dates.

    Args:
        from: str, date lower bound
        to: str, date upper bound
        location: str, where devices are located, can be "any"
        device: str, name of specific device, can be "any" to get all from specific location

    Return:
        json of data points
    """
    from_str  = request.args.get("from")
    to_str = request.args.get("to")
    location = request.args.get("location", default="any", type=str)
    device = request.args.get("device", default="any", type=str)

    # validation
    try:
        # datetime
        if "T" in from_str and "T" in to_str:
            from_date = datetime.strptime(from_str, "%Y-%m-%dT%H:%M:%S")
            to_date = datetime.strptime(to_str, "%Y-%m-%dT%H:%M:%S")

        # only date
        else:
            from_date = datetime.strptime(from_str, "%Y-%m-%d")
            to_date = datetime.strptime(to_str, "%Y-%m-%d")
            to_date = to_date.replace(hour=23, minute=59, second=59)


        if from_date > to_date or from_date > datetime.now():
            raise ValueError

        # clamp
        if to_date > datetime.now():
            to_date = datetime.now()
            log.info("clamped date to {}".format(to_date))

    except ValueError:
        log.error("malformed date")
        log.error(f"from={from_str}, to={to_str}")
        return jsonify([])

    from_str = from_date.strftime("\'%Y-%m-%dT%H:%M:%SZ\'")
    to_str = to_date.strftime("\'%Y-%m-%dT%H:%M:%SZ\'")

    query_str = ""
    result = None

    try:
        with connection() as client:
            if device == "any" and location != "any":
                query_str = 'SELECT * FROM "all" WHERE "location" = $location and time >= {} and time < {} ORDER BY time ASC'.format(from_str, to_str)
                result = client.query(query=query_str, bind_params={"location": location})

            elif device != "any" and location == "any":
                query_str = 'SELECT * FROM "all" WHERE "device" = $device and time >= {} and time < {} ORDER BY time ASC'.format(from_str, to_str)
                result = client.query(query=query_str, bind_params={"device": device})

            elif device == "any" and location == "any":
                query_str = 'SELECT * FROM "all" WHERE time >= {} and time < {} ORDER BY time ASC'.format(from_str, to_str)
                result = client.query(query=query_str)

            else:
                query_str = 'SELECT * FROM "all" WHERE "device" = $device and "location" = $location and time >= {} and time < {} ORDER BY time ASC'.format(from_str, to_str)
                result = client.query(query=query_str, bind_params={"device": device, "location": location})

        if len(result) == 0:
            log.info(f"no data, {query_str}")
            return jsonify([])

        points = list(result.get_points())

        return jsonify(points)


    except InfluxDBClientError:
        log.error("client error")
        log.error(query_str)
        log.error("result == {}".format(result))
        return jsonify([])



@app.route("/api/day")
def get_day_all():
    """Get all data points of a specific day.

    Args:
        date: str
        device: str, can be "any" for multiple devices

    Return:
        json of data points

    """
    date_str  = request.args.get("date")
    device = request.args.get("device", default="any", type=str)

    try:
        date_obj = datetime.strptime(date_str, "%Y-%m-%d")

    except ValueError:
        return jsonify([])

    if date_obj > datetime.now():
        date_obj = datetime.now().replace(hour=0, minute=0, second=0)
        log.info("clamped date to {}".format(date_obj))


    next_date = date_obj + timedelta(days=1)

    if next_date > datetime.now():
        next_date = datetime.now()
        log.info("clamped date to {}".format(next_date))

    date_str = date_obj.strftime("\'%Y-%m-%dT%H:%M:%SZ\'")
    next_str = next_date.strftime("\'%Y-%m-%dT%H:%M:%SZ\'")

    result = None

    try:
        with connection() as client:
            if device != "any":
                query_str = 'SELECT * FROM "all" WHERE "device" = $device and time >= {} and time <= {}'.format(date_str, next_str)
                result = client.query(query=query_str, bind_params={"device": device})
            else:
                query_str = 'SELECT * FROM "all" WHERE time >= {} and time <= {}'.format(date_str, next_str)
                result = client.query(query=query_str)




            points = list(result.get_points())

            return jsonify(points)


    except InfluxDBClientError:
        log.error(query_str)
        log.error("result == {}".format(result))
        return jsonify([])



@app.route("/api/devices")
def get_devices():
    """Get list of devices, data of which can be found in database

    Return:
        list of devices

    """
    query = 'SHOW TAG VALUES with key = "device"'
    try:

        with connection() as client:
            data = client.query(query=query)

            keys = data.keys()

            if len(keys) == 0:
                log.error("no devices in database")
                return jsonify([])

            devices_raw = list(data[keys[0]])
            devices = []

            for dev in devices_raw:
                devices.append(dev["value"])

            log.info(devices)
            return jsonify(devices)

    except Exception as err:
        log.exception("failed to connect to database")
        return jsonify([])


@app.route("/api/points", methods=["POST"])
def add_points():
    """Add list of data points to database.

    Format and types:
        [
            {
                "device": str,
                "location": str,
                "time": str (yyyy-mm-dd hh:mm:ss),
                "humidity": float,
                "temperature": float,
                "pressure": float
            },
        ]


    Return:
        {<severity>: <message>}, e.g. {"info": "operation successful"}

    """

    log.info("text: \n" + request.get_data(as_text=True))
    json_data = request.get_json()

    points = []
    log.info("points:")

    for item in json_data:
        point = {
            "measurement": "all",
            "tags": {
                "device": item.get("device", None),
                "location": item.get("location", None)
            },
            "time": item.get("time", datetime.now()),
            "fields": {
                "temperature": item.get("temperature", None),
                "humidity": item.get("humidity", None),
                "pressure": item.get("pressure", None)
            }

        }

        log.info("\n"+ json.dumps(point, indent=4))

        device = point["tags"]["device"]

        if device not in devices:
            res = make_response({"error": f'unknown device \"{device}\"'}, 400)
            log.error(f"{res.status} {res.get_json()}")
            return res

        # TODO: test
        time_diff = datetime.now() - devices[device]
        if time_diff.total_seconds() < config["device"][device]["data_interval"]:
            log.info(f'data point from {device} discarded')
            continue
        else:
            devices[device] = datetime.now()


        for key, value in point["tags"].items():
            if value is None:
                res = make_response({"error": f"missing field \"{key}\""}, 400)
                log.error(f"{res.status} {res.get_json()}")
                return res


        for key in list(point["fields"].keys()):
            if point["fields"][key] is None:
                point["fields"].pop(key)


        if len(point["fields"].keys()) == 0:
            res = make_response({"error": f"no data given"}, 400)
            log.error(f"{res.status} {res.get_json()}")
            return res

        points.append(point)


    with connection() as client:

        if client.write_points(points):
            res = make_response({"info": f"wrote {len(points)} points"}, 200)
            log.info(f"{res.status} {res.get_json()}")
            return res

        else:
            res = make_response({"error": f"failed to write data"}, 500)
            log.error(f"{res.status} {res.get_json()}")
            return res


