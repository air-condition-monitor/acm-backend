import requests


"""
To Run:

    start server:
        python src/entry.py

    in another terminal, run tests:
        pytest

"""

url = "http://localhost:5000/api/points"


def send(points):
    res = requests.post(url, json = points)
    print(res.json())

    return res


def test_single():
    point = {
        "device": "arduino",
        "location": "inside",
        "time": "2001-01-01 10:00:00",
        "humidity": 0.00,
        "temperature": 10.0,
        "pressure": 1.000
    }

    res = send([point])
    assert res.ok
    assert res.json() == {'info': 'wrote 1 points'}


def test_iso_time():
    point = {
        "device": "ruuvi",
        "location": "outside",
        "time": "2021-07-25T10:37:43+0000",
        "humidity": 0.00,
        "temperature": 10.0,
        "pressure": 1.000
    }

    res = send([point])
    assert res.ok
    assert res.json() == {'info': 'wrote 1 points'}


def test_multiple():
    points = [
        {
            "device": "arduino",
            "location": "inside",
            "time": "2001-01-01 10:00:00",
            "humidity": 0.00,
            "temperature": 10.0,
            "pressure": 1.000
        },
        {
            "device": "arduino",
            "location": "inside",
            "time": "2002-01-01 10:00:00",
            "humidity": 5.00,
            "temperature": 10.0,
            "pressure": 1.000
        }
    ]

    res = send(points)
    assert res.ok
    assert res.json() == {'info': 'wrote 2 points'}


def test_tag_missing():
    point = {
        "device": "arduino",
        "time": "2000-01-01 10:00:00",
        "humidity": 0.00,
        "temperature": 10.0,
        "pressure": 1.000
    }

    res = send([point])
    assert not res.ok
    assert res.json() == {'error': 'missing field "location"'}


def test_field_missing():
    point = {
        "device": "arduino",
        "location": "inside",
        "time": "2000-01-01 10:00:00",
        "humidity": 0.00,
        "pressure": 1.000
    }

    res = send([point])
    assert res.ok
    assert res.json() == {'info': 'wrote 1 points'}


def test_no_fields():
    point = {
        "device": "arduino",
        "location": "inside",
        "time": "2000-01-01 10:00:00",
    }

    res = send([point])
    assert not res.ok
    assert res.json() == {'error': 'no data given'}


