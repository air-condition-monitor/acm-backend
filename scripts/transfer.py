import sys
import os
import json
from datetime import datetime
import time
import requests


"""
ruuvidata:
{
    'temperature': 26.75,
    'humidity': 42.785,
    'pressure': 98155.0,
    'acceleration': {
        'x': 0.048,
        'y': 0.028,
        'z': 0.992
    },
    'voltage': 3.085,
    'datetime': '2020-08-23T23:51:15.930253',
    'mac': 'FA50248D8250'
}
"""

def main():

    if len(sys.argv) < 3:
        print("<port from> <port to>")
        return

    fromDate = "2021-08-18" #"2018-01-01"
    toDate = "2022-01-01"
    url = f"http://127.0.0.1:{sys.argv[1]}/api/range?from={fromDate}&to={toDate}"
    print(url)
    res = requests.get(url)

    data = res.json()

    """
    for p in data:
        print(p)
    """
    print("points", len(data))

    url = f"http://127.0.0.1:{sys.argv[2]}/api/points"
    res = requests.post(url, json=data)


if __name__ == "__main__":

    main()

