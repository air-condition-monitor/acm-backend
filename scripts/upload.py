# influx 1.x
from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError

# influx 2.0
#from influxdb_client import InfluxDBClient, Point
#from influxdb_client.client.write_api import ASYNCHRONOUS

import configparser
import os
import sys
import json
import toml


config = toml.load(f"../configs/config.toml", _dict=dict)["database"]


def get_client():
    client = InfluxDBClient(host="127.0.0.1",
                            port=int(config["influx1"]["port"]),
                            database=config["influx1"]["name"])

    client.create_database(config["influx1"]["name"])
    client.switch_database(config["influx1"]["name"])

    return client


def query_points():
    """
    print("connect")
    client = InfluxDBClient.from_config_file("./configs/database.ini")
    query_api = client.query_api()

    tables = query_api.query('SELECT "all" FROM "acm" WHERE time < now()')

    for table in tables:
        print(table)
        for record in table.records:
            print(record.values)
    """
    client = get_client()

    result = None

    try:
        query_str = 'SELECT * FROM "all" ORDER BY time ASC'
        result = client.query(query=query_str)

    except InfluxDBClientError:
        print("client error")
        print(query_str)
        print("result == {}".format(result))
        return []

    if len(result) == 0:
        log.error(query_str)
        return []

    return list(result.get_points())





    client.close()




def read_points(path):
    points = []

    f =  open(path)
    data = json.load(f)

    for item in data:

        point = {
            "measurement": "all",
            "tags": {
                "device": item["device"],
                "location": item["location"],
            },
            "time": item["time"],
            "fields": {
                "humidity": item.get("humidity", None),
                "pressure": item.get("pressure", None),
                "temperature": item.get("temperature", None)
            }

        }

        points.append(point)

    f.close()

    return points


def write_points(points):
    """
    client = InfluxDBClient.from_config_file("./configs/database.ini")

    write_api = client.write_api(write_options=ASYNCHRONOUS)

    result = write_api.write(bucket ="acm", org = "org", record = points)

    print(result.get())
    """

    client = get_client()

    if client.write_points(points):
        print("data written successfully to db")
    else:
        print("failed to write to db")

    client.close()


def print_db():
    client = get_client()
    result = client.query(query='SELECT * FROM "all" ORDER BY time DESC')

    points = list(result.get_points())

    for p in points:
        print(p)


def main():

    if len(sys.argv) != 2:
        print("specify datafile to upload")
        return

    points = read_points(sys.argv[1])
    write_points(points)



if __name__ == "__main__":
    main()



