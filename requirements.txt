Flask==2.0.1
Flask-Cors==3.0.10
gunicorn==20.1.0
influxdb==5.3.1
pytest==6.2.4
requests==2.26.0
toml==0.10.2
