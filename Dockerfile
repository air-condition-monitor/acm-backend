FROM python:3.8
WORKDIR /backend

#RUN useradd dockeruser
COPY setup.py ./setup.py
COPY configs/config.toml configs/log.toml ./configs/
COPY src ./src

ARG logdir=/var/log/backend
RUN mkdir -p $logdir
RUN touch $logdir/gunicorn.log
RUN touch $logdir/backend.log


RUN python3 -m venv venv
RUN ./venv/bin/python -m pip install -U pip
RUN ./venv/bin/python -m pip install -e .

ENV FLASK_ENV production

EXPOSE 5000
CMD ["/backend/venv/bin/gunicorn", \
    "--workers", \
    "3", \
    "--log-file", \
    "/var/log/backend/gunicorn.log", \
    "-b", \
    ":5000", \
    "app:app"]
